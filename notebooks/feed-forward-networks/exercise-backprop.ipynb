{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Backpropagation - Exercise: Computational Graphs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements) \n",
    "  * [Knowledge](#Knowledge) \n",
    "  * [Modules](#Python-and-System-Packages)\n",
    "* [Exercise: Pen and Paper Backpropagation](#Pen-and-Paper-Backpropagation)\n",
    "* [Exercise: Implement the Example](#Implement-the-Example)\n",
    "* [Summary and Outlook](#Summary-and-Outlook)\n",
    "* [Licenses](#Licenses) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "In this exercise, you will apply the backpropagation algorithm to computational graphs. Backpropagation is the method we use to calculate the gradients of all learnable parameters in an artificial neural network efficiently and conveniently. Approaching the algorithm from the perspective of computational graphs gives a good intuition about its operations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Requirements\n",
    "\n",
    "### Knowledge\n",
    "You should already be familiar with the backpropagation algorithm, as well as differential calculus. The following resources are recommend if this topic is new to you:\n",
    "\n",
    "- Chapter 6.5 of [Deep Learning](http://www.deeplearningbook.org/contents/ml.html) by Ian Goodfellow gives a brief introduction into the field\n",
    "- MOOC [Calculus](https://www.khanacademy.org/math/calculus-1) from Khan Academy\n",
    "\n",
    "### Python and System Packages\n",
    "To run that Notebook you should have the `graphviz` package installed on your System. Look at the [Graphviz-Website](https://graphviz.gitlab.io/download/) for installation guidance. Further, the Python module `Digraph` is only working with Jupyter Notebook (not Lab) at the moment. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# External Modules\n",
    "import numpy as np\n",
    "import hashlib\n",
    "from graphviz import Digraph"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "def round_and_hash(value, precision=4, dtype=np.float32):\n",
    "    \"\"\" \n",
    "    Function to round and hash a scalar or numpy array of scalars.\n",
    "    Used to compare results with true solutions without spoiling the solution.\n",
    "    \"\"\"\n",
    "    rounded = np.array([value], dtype=dtype).round(decimals=precision)\n",
    "    hashed = hashlib.md5(rounded).hexdigest()\n",
    "    return hashed"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pen and Paper Backpropagation\n",
    "Given the function $ f(a,b,c) $ as a computational graph with the following values for the parameters:\n",
    "\n",
    "$$\n",
    "a = 2 \\\\\n",
    "b = e \\; (Euler \\;number, \\;b \\approx 2.7183) \\\\\n",
    "c = 3\n",
    "$$\n",
    "\n",
    "**Tasks:**\n",
    "- Give an equation that expresses the graph.\n",
    "- Calculate the partial derivatives $ \\frac {\\partial out} {\\partial a}$, $ \\frac {\\partial out} {\\partial b}$. and $ \\frac {\\partial out} {\\partial c}$ using calculus on pen & paper\n",
    "- For b use the the true $Euler \\;number$ and not the approximation\n",
    "- Assign your results to variables `partial_a`,`partial_b` and `partial_c` to verify your solution with the software test below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# creating empty graph ad set some attributes\n",
    "f = Digraph('computat:onal_graph', filename='graph_clean.gv')\n",
    "f.attr(rankdir='LR')\n",
    "f.attr('node', shape='circle')\n",
    "\n",
    "# create the graph\n",
    "f.node('a')\n",
    "f.node('b')\n",
    "f.node('c')\n",
    "f.edge('a', '+', label=' ')\n",
    "f.edge('b', 'ln', label=' ')\n",
    "f.edge('ln', '+', label=' ')\n",
    "f.edge('+','* ', label=' ')\n",
    "f.edge('c','* ')\n",
    "f.edge('* ', '*')\n",
    "f.edge('1/3 ', '*')\n",
    "f.edge('*','1/x')\n",
    "f.edge('1/x','out')\n",
    "\n",
    "f"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "partial_a = 42 ### assign what u have calculated\n",
    "partial_b = 42 ### assign what u have calculated\n",
    "partial_c = 42 ### assign what u have calculated"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "assert round_and_hash(partial_a) == '2fb0a82d3fe965c8a0d9ce85970058d8'\n",
    "assert round_and_hash(partial_b) == 'b6bfa6042c097c90ab76a61300d2429a'\n",
    "assert round_and_hash(partial_c) == '2fb0a82d3fe965c8a0d9ce85970058d8'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Implement the Example\n",
    "\n",
    "**Task:**\n",
    "\n",
    "In the pen and paper exercise, you calculated the partial derivatives for some specific values of $a$, $b$, and $c$ of the graph $f(a,b,c)$. Your task now is to generalize that solution. Implement the used functions $+$, $*$ and $1/x$ and their derivatives. Chain the functions to calculate $f(a,b,c)$ in a forward pass and $ \\frac {\\partial out} {\\partial a}$, $ \\frac {\\partial out} {\\partial b}$, $ \\frac {\\partial out} {\\partial c}$ in the backward pass for arbitrary values of $a$, $b$, and $c$.\n",
    "\n",
    "**Hint:**\n",
    "\n",
    "Complete function:\n",
    "\n",
    "$$\n",
    "out = powminusone(mul_2(mul_1(add(a, ln(b)), c), \\frac{1}{3}))\n",
    "$$\n",
    "\n",
    "So...\n",
    "\n",
    "Remember chain rule:\n",
    "$$\n",
    "\\frac{\\partial out}{\\partial a} = \\frac{\\partial out}{\\partial powminusone} * \\frac{\\partial powminusone}{\\partial mul_2} *  \\frac{\\partial mul_2}{\\partial mul_1} * \\frac{\\partial mul_1}{\\partial add} * \\frac{\\partial add}{\\partial a}\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\frac{\\partial out}{\\partial b} = \\frac{\\partial out}{\\partial powminusone} * \\frac{\\partial powminusone}{\\partial mul_2} *  \\frac{\\partial mul_2}{\\partial mul_1} * \\frac{\\partial mul_1}{\\partial add} * \\frac{\\partial add}{\\partial ln(b)} * \\frac{\\partial ln(b)}{\\partial b}\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\frac{\\partial out}{\\partial c} = \\frac{\\partial out}{\\partial powminusone} * \\frac{\\partial powminusone}{\\partial mul_2} *  \\frac{\\partial mul_2}{\\partial mul_1} * \\frac{\\partial mul_1}{\\partial c}\n",
    "$$\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "### Your implementation here\n",
    "\n",
    "# 1/x is the same like \"x to the power of minus one\"\n",
    "def pow_minus_one(x, derivative=False):\n",
    "    if derivative:\n",
    "        raise NotImplementedError()\n",
    "    else:\n",
    "        raise NotImplementedError()\n",
    "        \n",
    "def addition(x,y, derivative=False):\n",
    "    if derivative:\n",
    "        raise NotImplementedError()\n",
    "    else:\n",
    "        raise NotImplementedError()    \n",
    "\n",
    "def multi(x,y, derivative=False):\n",
    "    if derivative:\n",
    "        # Derivative with respect to x\n",
    "        raise NotImplementedError()\n",
    "    else:\n",
    "        raise NotImplementedError()    \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "a = 2\n",
    "b = np.e\n",
    "c = 3\n",
    "\n",
    "### complete the forward pass:\n",
    "# forward = pow_minus_one(multi(...\n",
    "\n",
    "### complete the backwards pass for \"a\"\n",
    "#part_a = (pow_minus_one(multi(multi(addition(a,np.log(b)),c),1/3), derivative=True) \n",
    "#        * multi(multi(addition(a,np.log(b)),c),1/3, derivative=True)\n",
    "#        * ...\n",
    "\n",
    "### complete the backwards pass for \"b\"\n",
    "#part_b = ...\n",
    "\n",
    "### complete the backwards pass for \"c\"\n",
    "#part_c = ...\n",
    "\n",
    "### print for self control\n",
    "#print(part_a)\n",
    "#print(part_b)\n",
    "#print(part_b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "assert round_and_hash(forward) == '8565183eaf4b5c6356d6abb81b8e139d'\n",
    "\n",
    "assert round_and_hash(part_a) == '2fb0a82d3fe965c8a0d9ce85970058d8'\n",
    "assert round_and_hash(part_b) == 'b6bfa6042c097c90ab76a61300d2429a'\n",
    "assert round_and_hash(part_c) == '2fb0a82d3fe965c8a0d9ce85970058d8'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary and Outlook\n",
    "\n",
    "In this exercise you implemented backpropagation in a computational graph. You applied the chain rule to compute the partial derivative of the output with respect to each node in the graph.\n",
    "In the upcoming exercises, you'll use the same backpropagation method to learn the gradients of parameters in a neural network."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "Exercise: Computational Graphs <br/>\n",
    "by Benjamin Voigt <br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 Benjamin Voigt\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
