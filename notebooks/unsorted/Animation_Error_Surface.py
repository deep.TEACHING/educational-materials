# -*- coding: utf-8 -*-
"""
Created on Wed Jun  5 11:18:27 2019

@author: Admin
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.patches as mpatches
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.lines as mlines



def plot_err_surface(Limit,XData,YData,errfunc,model):
    
    Num = 50
    P1 = np.linspace(Limit[0],Limit[1],num=Num)
    P2 = np.linspace(Limit[2],Limit[3],num=Num)
    P1_mesh , P2_mesh = np.meshgrid(P1,P2)
    
    Error = np.zeros([len(P1),len(P2)])
    
    for i in range(Num):
        for j in range(Num):
            Error[i,j] = errfunc(YData,XData,[P1_mesh[i,j],P2_mesh[i,j]],model)
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    surf = ax.plot_surface(P1_mesh,P2_mesh,Error,rstride = 1, cstride=1, cmap='viridis', linewidth=0, antialiased=False) 
    fig.colorbar(surf)
    ax.set_xlabel('Para_1')
    ax.set_ylabel('Para_2')
    


def optimizer_animation(Limit,XData,YData,errfunc,model,para_hist,err_hist,opt_labels=None):
    
    Num = 10
    P1 = np.linspace(Limit[0],Limit[1],num=Num)
    P2 = np.linspace(Limit[2],Limit[3],num=Num)
    P1_mesh , P2_mesh = np.meshgrid(P1,P2)
    
    Error = np.zeros([len(P1),len(P2)])
    
    for i in range(Num):
        for j in range(Num):
            Error[i,j] = errfunc(YData,XData,[P1_mesh[i,j],P2_mesh[i,j]],model)
    
    fig = plt.figure()
    ax = p3.Axes3D(fig)
    surf = ax.plot_surface(P1_mesh,P2_mesh,Error,rstride = 1, cstride=1, cmap='viridis', linewidth=0, antialiased=False, alpha=0.4) 
    fig.colorbar(surf)
    ax.set_xlabel('Para_1')
    ax.set_ylabel('Para_2')
    
    
    colors = ['r','b','g','c','m','y','k']
 
    
    def err_dot(i):
        ax.plot([para_hist[i,0]],[para_hist[i,1]],[err_hist[i]],'ro')
        
    def err_dot_multi(i):
        for j in range(len(err_hist)):
            ax.plot([para_hist[j,i,0]],[para_hist[j,i,1]],[err_hist[j,i]],str(colors[j])+'o',MarkerSize=6,label='opti'+str(j))
            print('New Dot')
            
    if len(err_hist.shape)>=2:
    
        dot = err_dot_multi
        epochs = len(err_hist[1])
        print('Training epochs: ',epochs)
        
        legend_items = []
        for j in range(len(err_hist)):
            
            if opt_labels == None:
                labels = 'Opt '+str(j)
                legend_items.append(mpatches.Patch(color=colors[j],label=labels))
            else:
                labels = opt_labels[j]
                legend_items.append(mpatches.Patch(color=colors[j], label=labels))
                
        
    
    else:                        
        dot=err_dot
        epochs = len(err_hist)
        print('Training epochs: ',epochs)
        
        if opt_labels == None:
            labels = 'Opt '+str(j)
        else:
            labels = opt_labels[0]
            
                                
        legend_items = [mpatches.Patch(color='r', label=labels)]
    

    DotAnimation = animation.FuncAnimation(fig, dot, epochs, interval=400, blit=True, repeat=True)   
    

    plt.legend(handles=legend_items)
    plt.show()
    return DotAnimation







