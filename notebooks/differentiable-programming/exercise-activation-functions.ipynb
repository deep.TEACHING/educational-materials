{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Activation Functions (Differentiable Programming)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements)\n",
    "  * [Prerequisites](#Prerequisites)\n",
    "  * [Python Modules](#Python-Modules)\n",
    "* [Theory](#Theory)\n",
    "  * [Activation Functions](#Activation-Function)\n",
    "* [Exercises](#Exercises)\n",
    "    * [The Model Class](#The-Model-Class)\n",
    "    * [The Optimizer Class](#The-Optimizer-Class)\n",
    "    * [Training](#Training)\n",
    "    * [Exercise - Modify the Optimizer Class](#Exercise---Modify-the-Optimizer-Class)\n",
    "    * [Plot the Gradients](#Plot-the-Gradients)\n",
    "    * [Exercise - Modify the Net Class](#Exercise---Modify-the-Net-Class)\n",
    "    * [Freestyle Exercise](#Freestyle-Exercise)\n",
    "* [Literature](#Literature)\n",
    "* [Licenses](#Licenses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "Ever heared about the *vanishing gradient* problem? This notebook gives a brief introduction into activation functions and how they are connected with it. During the exercises you will train a small, yet deep network and visualize the gradients during training."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Requirements\n",
    "\n",
    "### Knowledge\n",
    "\n",
    "- Gradient Descent\n",
    "- Backpropagation\n",
    "\n",
    "### Prerequisites\n",
    "\n",
    "To solve this notebook you should either: \n",
    "* be familiar with the neural net framework you've been building in the 'Differentiable Programming' course so far. If dp.py is located in the same folder as this notebook, you can access it as a module with `import dp`\n",
    "* or be familiar with any other deep learning framework (e.g. PyTorch, which is also introduced in this course) where you can manually access the gradients\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Python Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from sklearn import datasets\n",
    "from sklearn.decomposition import PCA\n",
    "\n",
    "import dp\n",
    "from dp import Model,Node"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Theory\n",
    "\n",
    "### Recap Backpropagation\n",
    "\n",
    "Learning of (deep) neural networks relies on the backpropagation algorithm.\n",
    "\n",
    "Imagine a network with three hidden layers, each consisting of only one neuron:\n",
    "\n",
    "![internet connection needed](https://gitlab.com/deep.TEACHING/educational-materials/raw/master/media/klaus/3-hidden-layer-single-neuron.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the net output of our our last activation function $a_3$ is not equal to our desired output, given by our training data, then we adjust the weights $w_{1:3}$ and the bias $b_{1:3}$ with the following rules:\n",
    "\n",
    "* First we calculate the error between our output and the true label:\n",
    "    * $error = cost(a_3, y_{true})$\n",
    "* Second, we calculate the partial derivatives for the weights $\\frac{\\partial error}{\\partial w_j}$ and the bias $\\frac{\\partial error}{\\partial b_j}$ to find out in which direction we have to adjust them in order to lower the costs ($\\alpha$ the learning rate), e.g.:\n",
    "    * $w_1 \\leftarrow w_1 - \\alpha \\frac{\\partial error}{\\partial w_1}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To calculate $ \\frac{\\partial error}{\\partial w_1}$ we use the cain rule:\n",
    "\n",
    "$$\n",
    " \\frac{\\partial error}{\\partial w_1} =  \\frac{\\partial error}{\\partial a_3} \\cdot  \\frac{\\partial a_3}{\\partial a_2} \\cdot  \\frac{\\partial a_2}{\\partial a_1} \\cdot  \\frac{\\partial a_1}{\\partial w_1}\n",
    "$$\n",
    "\n",
    "We see, that $\\frac{\\partial error}{\\partial w_1}$ heavily depends on the derivatives of the activation functions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Activation functions\n",
    "\n",
    "Non-linear functions are one of the core functionalities of neural networks. Only by applying a non-linear function (e.g. $sigmoid$, $tanh$, $relu$) onto a linear combination of the features, we receive another (and hopefully linear seperable) representation of the features.\n",
    "\n",
    "#### Sigmoid\n",
    "\n",
    "For a long time, the sigmoid was used, not only in the last layer for classification, but also in the hidden layers as activation function. Let us have a look at it and its derivative:\n",
    "\n",
    "$$\n",
    "sigmoid(z) = \\frac{1}{1 + exp(-z)}\n",
    "$$\n",
    "\n",
    "and\n",
    "\n",
    "$$\n",
    "\\frac{\\partial sigmoid(z)}{\\partial z} = sigmoid(z) \\cdot (1- sigmoid(z))\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sigmoid(z):\n",
    "    return 1 / (1 + np.exp(-z))\n",
    "\n",
    "z = np.linspace(-10,+10,100)\n",
    "plt.plot(z, sigmoid(z), linewidth=3, label='sigmoid(z)')\n",
    "plt.plot(z, sigmoid(z)*(1-sigmoid(z)), linestyle='--', linewidth=2, label='derivative sigmoid(z)')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see $\\frac{\\partial sigmoid(z)}{\\partial z} \\in ]0, \\frac{1}{4}]$. The maximum it can be is $\\frac{1}{4}$. So for our example network the highest value we can get is:\n",
    "\n",
    "$$\n",
    " \\frac{\\partial error}{\\partial w_1} =  \\frac{\\partial error}{\\partial a_3} \\cdot \\frac{1}{4} \\cdot \\frac{1}{4} \\cdot \\frac{1}{4}\n",
    "$$\n",
    "\n",
    "In short: the deeper the network, the more the error vanishes as we propagate it back to the first layers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Tangens Hyperbolicus\n",
    "\n",
    "1998 the $tanh(z)$ was proposed to be supirior to the $sigmoid(z)$ [LEC98]. Let us have a look at it and its derivative:\n",
    "\n",
    "$$\n",
    "\\frac{\\partial tanh(z)}{\\partial z} = 1 - tanh(z)^2\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "z = np.linspace(-10,+10,100)\n",
    "plt.plot(z, np.tanh(z), linewidth=3, label='tanh')\n",
    "plt.plot(z, 1-np.tanh(z)**2, linestyle='--', linewidth=2, label='derivative tanh')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here our derivative is in the range of $]0, 1[$. So if we are lucky, our error does not vanish when we propagate it back to the first layers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " #### Rectified Linear Units (ReLU)\n",
    " \n",
    " Despite beeing superior to th $sigmoid$ function, the $tanh$ still shares another problem with it: If we are not very close to $0$, the function saturates and the derivatives become almost $0$. To counter this problem, the $ReLU$ function can be used:\n",
    " \n",
    "$$\n",
    "ReLU(z) = max(0,z)\n",
    "$$\n",
    "\n",
    "and\n",
    "\n",
    "$$\n",
    "\\frac{\\partial ReLU(z)}{\\partial z} = 0 \\text{ if } z < 0; 1 \\text{ if } z > 0\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def relu(z):\n",
    "    return np.maximum(z, np.zeros_like(z))\n",
    "\n",
    "z = np.linspace(-2,+2,100)\n",
    "plt.plot(z, relu(z), linewidth=3, label='relu(z)')\n",
    "plt.plot(z, np.sign(relu(z)), linestyle='--', linewidth=2, label='derivative relu(z)')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The range where $\\frac{\\partial ReLU(z)}{\\partial z} = 1$ (infinitely points) is a lot bigger than for $\\frac{\\partial tanh(z)}{\\partial z}$ (only at one point)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Problems to Note when using ReLU\n",
    "\n",
    "ReLU can result in dead neurons when the partial derivative is $0$. this can especially be a problem when using only few neurons in a layer. On the other side, one can argue, that these dead neurons enforce sparsity of the network and lead to less overfitting, as long as they are not dead for every training example.\n",
    "\n",
    "Another problem is that you can run into overflow fast when computing the forward pass. This is not a problem for $tanh$ and $sigmoid$ as their output is always between $]-1, +1[$, resp. $]0, +1[$. \n",
    "\n",
    "To tackle overflow there exist several techniques like restricting the output of ReLU to a fixed value, e.g. $2$. This is also called *clipping*. It is also a good idea to normalize the inputs to have mean $\\mu = 0$ and standard deviation $\\sigma=1$. The latter is also a necessary step to avoid saturation when using $tanh$ or $sigmoid$, so it is not even an essential step exclusive to $ReLU$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises\n",
    "\n",
    "In order to show the effect of vanishing gradient, we will build a deep, yet simple neural network."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data\n",
    "\n",
    "We use the iris dataset and from it only the first 100 examples, which contain only classes 0 and 1 so we have a binary classification problem for simplicity."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Load iris dataset\n",
    "iris = datasets.load_iris()\n",
    "\n",
    "### 0-100 only contains class 0 and 1 for simplicity\n",
    "X = iris.data[:100]\n",
    "y = iris.target[:100]\n",
    "\n",
    "### Feature scaling\n",
    "X = (X - X.mean(axis=0)) / X.std(axis=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The Model Class\n",
    "\n",
    "Below you can see a starting point for an implementation for a network class with the following limitations:\n",
    "- only 1 hidden layer\n",
    "- hidden layer has fixed size of 10 neurons\n",
    "- fixed activation function $tanh$ for the hidden layer\n",
    "\n",
    "For now, leave this part as it is and complete the rest of the notebook first, so you can check if you successfully solved the other exercises.\n",
    "\n",
    "After everything runs fine with this static network, come back here to enhance the model class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "class Net(Model):    \n",
    "    def __init__(self, n_features, n_hidden_neurons, n_layers, act_func):\n",
    "        super(Net, self).__init__()\n",
    "        self.hidden = self.Linear_Layer(n_features, 10, \"h0\")\n",
    "        self.out = self.Linear_Layer(10, 1, \"h1\")\n",
    "        \n",
    "    def loss(self, x, y):\n",
    "        if not type(y) == Node:\n",
    "            y = Node(y)\n",
    "        out = self.forward(x)\n",
    "        loss = -1 * (y * out.log() + (1 - y) * (1 - out).log())\n",
    "        return loss.sum()\n",
    "    \n",
    "    def forward(self, x):\n",
    "        if not type(x) == Node:\n",
    "            x = Node(x)\n",
    "        x = self.hidden(x).tanh()\n",
    "        x = self.out(x).sigmoid()\n",
    "        return x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The Optimizer Class\n",
    "\n",
    "Below you can see the `Optimizer` and `SGD` class from `dp.py`. We copied it here because we are going to modify it later to save the gradients so we can access them after the training."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "class Optimizer(object):\n",
    "    \n",
    "    def __init__(self, model, x_train=None, y_train=None, hyperparam=dict(), batch_size=128):\n",
    "        self.model = model\n",
    "        self.x_train = x_train\n",
    "        self.y_train = y_train\n",
    "        self.batch_size=batch_size\n",
    "        self.hyperparam = hyperparam\n",
    "        self._set_param()\n",
    "        self.grad_stores = [] # list of dicts for momentum, etc. \n",
    "        \n",
    "    def random_batch(self):\n",
    "        n = self.x_train.shape[0]\n",
    "        indices = np.random.randint(0, n, size=self.batch_size)\n",
    "        return Node(self.x_train[indices]), Node(self.y_train[indices])\n",
    "\n",
    "    def train(self, steps=1000, print_each=100):\n",
    "        raise NotImplementedError()\n",
    "\n",
    "    def _train(self, steps=1000, num_grad_stores=0, print_each=100):\n",
    "        assert num_grad_stores in (0,1,2)\n",
    "        model = self.model\n",
    "        if num_grad_stores>0:\n",
    "            x, y = self.random_batch()\n",
    "            grad, loss = model.get_grad(x, y)\n",
    "            self.grad_stores = [dict() for _ in range(num_grad_stores)]\n",
    "        for grad_store in self.grad_stores:\n",
    "            for g in grad:\n",
    "                grad_store[g] = np.zeros_like(grad[g])\n",
    "                \n",
    "        param = model.get_param()\n",
    "        print(\"iteration\\tloss\")    \n",
    "        for i in range(1, steps+1):\n",
    "            x, y = self.random_batch()\n",
    "            grad, loss = model.get_grad(x, y)\n",
    "            if i%print_each==0 or i==1:\n",
    "                print(i, \"\\t\",loss.value[0,0])       \n",
    "                \n",
    "            for g in grad:\n",
    "                self._update(param, grad, g, i)\n",
    "            \n",
    "            model.set_param(param)\n",
    "        \n",
    "        return loss.value\n",
    "\n",
    "    \n",
    "    \n",
    "class SGD(Optimizer):\n",
    "    \n",
    "    def __init__(self, model, x_train=None, y_train=None, hyperparam=dict(), batch_size=128):\n",
    "        super(SGD, self).__init__(model, x_train, y_train, hyperparam, batch_size)\n",
    "        \n",
    "    def _set_param(self):\n",
    "        self.alpha = self.hyperparam.get(\"alpha\", 0.001)\n",
    "\n",
    "    def _update(self, param, grad, g, i):\n",
    "        param[g] -= self.alpha * grad[g]    \n",
    "             \n",
    "    def train(self, steps=1000, print_each=100):\n",
    "        return self._train(steps, num_grad_stores=1, print_each=print_each)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Training\n",
    "\n",
    "Now start the training. Note that we already pass the arguments `n_hidden_layers`, `n_hidden_neurons` and  `act_func` to the `Net` class when initializing our network, altohugh, they have no function yet as the number of layers, neurons and types of activation functions inside `Net` are statically coded for now.\n",
    "\n",
    "The training should succeed and yield 100 % accuracy after 100 epochs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "n_epochs = 100\n",
    "n_hidden_layers = 1 # number of hidden layers\n",
    "n_hidden_neurons = 1 # number of neurons per hidden layer\n",
    "n_features = X.shape[1]\n",
    "act_func = 'sigmoid'\n",
    "\n",
    "grad_w = np.zeros(shape=(n_hidden_layers+1, n_epochs)) ### n_hidden_layers + 1 (for output)\n",
    "net = Net(n_features, n_hidden_neurons, n_hidden_layers, act_func)\n",
    "\n",
    "optimiser = SGD(\n",
    "    net,\n",
    "    x_train=X,\n",
    "    y_train=y\n",
    ")\n",
    "optimiser.train(steps=n_epochs,print_each=n_epochs//10);\n",
    "\n",
    "y_pred = net.forward(X).value\n",
    "\n",
    "acc = (len(y) - np.sum(np.abs(y - y_pred.round().flatten()))) / len(y)\n",
    "print('accuracy on the training data after training: ', acc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise - Modify the Optimizer Class\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Modify the `Optimizer` and/or `SGD` class, so the **mean** of the **absolute** values of the partial derivatives of all wheights in one layer are saved in the variable `grad_w` (defined in the cell above):\n",
    "\n",
    "* The first dimension of `grad_w` corresponds to the layer-number\n",
    "* The second dimension correpsonds to the epoch\n",
    "\n",
    "**Hint:**\n",
    "\n",
    "If a quick and dirty solution is ok for you, directly access `grad_w` as a global variable inside `Optimizer._train`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot Gradients\n",
    "\n",
    "With the new implementation in place, execute the training cell again to populate the log of gradients `grad_w`.\n",
    "\n",
    "If everything is correct, executing the cell below should plot 2 graphs like the following picture:\n",
    "\n",
    "![internet connection needed](https://gitlab.com/deep.TEACHING/educational-materials/raw/master/media/klaus/abs_gradients_2_layers.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n_epochs = 100\n",
    "n_hidden_layers = 1 # number of hidden layers\n",
    "n_hidden_neurons = 1 # number of neurons per hidden layer\n",
    "n_features = X.shape[1]\n",
    "act_func = 'sigmoid'\n",
    "\n",
    "grad_w = np.zeros(shape=(n_hidden_layers+1, n_epochs)) ### n_hidden_layers + 1 (for output)\n",
    "net = Net(n_features, n_hidden_neurons, n_hidden_layers, act_func)\n",
    "\n",
    "optimiser = SGD(\n",
    "    net,\n",
    "    x_train=X,\n",
    "    y_train=y\n",
    ")\n",
    "optimiser.train(steps=n_epochs,print_each=n_epochs//10);\n",
    "\n",
    "y_pred = net.forward(X).value\n",
    "\n",
    "acc = (len(y) - np.sum(np.abs(y - y_pred.round().flatten()))) / len(y)\n",
    "print('accuracy on the training data after training: ', acc)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "fig, axs = plt.subplots(nrows=n_hidden_layers+1, ncols=1, figsize=(16,16))\n",
    "for i in range(n_hidden_layers):\n",
    "    axs[i].set_title('absolute mean gradients of hidden layer number [{}]'.format(i))\n",
    "    axs[i].plot(np.linspace(0,n_epochs-0, n_epochs-0), grad_w[i,0:])\n",
    "axs[-1].set_title('absolute mean gradients of the output layer')\n",
    "axs[-1].plot(np.linspace(0,n_epochs-0, n_epochs-0), grad_w[i,0:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise - Modify the Net Class\n",
    "\n",
    "**Task:**\n",
    "\n",
    "Modify the `Net` class:\n",
    "\n",
    "* Your class should be able to produce nets with different numbers of hidden layers according to the parameter `n_layers`.\n",
    "* The number of neurons in each layer should depend on the parameter `n_hidden_neurons`.\n",
    "* Depending on the string-parameter `act_func`, your network should either use `tanh`, `sigmoid` or `relu`.\n",
    "    * Remember that the activation function of the very last layer should still always be `sigmoid`\n",
    "\n",
    "When you are finished with the changes, try networks with more layers.\n",
    "For example, you can expect a network with 10 layers each using the `sigmoid` activation function to not learn at all. Plotting the gradients should show vanishing gradients towards the first layers like in the following plot:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Sample Plot:**\n",
    "\n",
    "Plot showing the absolute gradients mean of each layer. Here with sigmoid as the activation function and 10 layers. Note the y-axis scale:\n",
    "\n",
    "![internet connection needed](https://gitlab.com/deep.TEACHING/educational-materials/raw/master/media/klaus/abs_gradients_10_layers.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Freestyle Exercise\n",
    "\n",
    "Experiment with number of layers and activation functions and see if you can get results as proof for the statements made in the theory chapter of this notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Literature\n",
    "\n",
    "<table>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"LEC98\"></a>[LEC98]\n",
    "        </td>\n",
    "        <td>\n",
    "            LECUN, Yann A., et al. Efficient backprop. In: Neural networks: Tricks of the trade. Springer, Berlin, Heidelberg, 2012. S. 9-48.\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "_Activation Functions (Differentiable Programming)_ <br/>\n",
    "by Klaus Strohmenger <br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2019 Klaus Strohmenger\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
