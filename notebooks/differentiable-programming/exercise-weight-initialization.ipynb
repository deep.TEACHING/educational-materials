{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Weight Initialization (Differentiable Programming)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements)\n",
    "  * [Prerequisites](#Prerequisites)\n",
    "  * [Python Modules](#Python-Modules)\n",
    "* [Variance](#Variance)\n",
    "* [Weight initialization by considering only the forward pass](#Weight-initialization-by-considering-only-the-forward-pass)\n",
    "* [Exercises](#Exercises)\n",
    "  * [Forward pass](#Forward-pass)\n",
    "  * [Visualise the activations](#Visualise-the-activations)\n",
    "  * [Xavier Initialization](#Xavier-Initialization)\n",
    "  * [Using the neural net framework](#Using-the-neural-net-framework)\n",
    "  * [Implement Initializer](#Implement-Initializer)\n",
    "* [Literature](#Literature)\n",
    "* [Licenses](#Licenses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "This notebook deals with parameter initialization in neural nets. Weights that start off too small or too large can cause gradients to vanish or explode, which is detrimental to the learning process. **Xavier initialization** aims to keep activations and gradients flowing in the forward and backward pass.\n",
    "\n",
    "In this notebook, you'll compare different initialization techniques and study their effect on the network. Finally you'll implement a mechanism for custom weight initialization for the neural net library you've been building in this course."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Requirements\n",
    "\n",
    "### Knowledge\n",
    "A recommended read on network initialization is the blog post [Initialization of deep networks](http://deepdish.io/2015/02/24/network-initialization/) by Gustav Larsson [(#LAR15)](#LAR15)\n",
    "\n",
    "### Prerequisites\n",
    "This notebook uses the neural net framework you've been building in the 'Differentiable Programming' course - but you can use the implementation in `dp.py`. If dp.py is located in the same folder as this notebook, you can access it as a module with `import dp`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Python Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from sklearn import datasets,preprocessing\n",
    "from dp import Model,Node,SGD, Adam"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variance\n",
    "\n",
    "The variance of the product between two independent variables is:\n",
    "\n",
    "$$\n",
    "{\\rm Var}(XY) = E(X^2Y^2) − (E(XY))^2={\\rm Var}(X){\\rm Var}(Y)+{\\rm Var}(X)(E(Y))^2+{\\rm Var}(Y)(E(X))^2\n",
    "$$\n",
    "\n",
    "Goodman, Leo A., \"On the exact variance of products,\" Journal of the American Statistical Association, December 1960, 708–713.\n",
    "\n",
    "with zero-mean variables: \n",
    "$E(X) = E(Y) = 0$\n",
    "this is\n",
    "\n",
    "$$\n",
    "{\\rm Var}(XY) = {\\rm Var}(X){\\rm Var}(Y)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Weight initialization by considering only the forward pass\n",
    "- weight matrix $W$ consists of $m$ \n",
    "  - column vectors $\\vec w_i$ (neuron weights for a hidden neuron $i$, $m$ hiddens for the layer in total)\n",
    "  - each element was drawn from an IID Gaussian with variance $var(W)$.\n",
    "- input vector of one example (also hidden vector) $\\vec x^T$ with expected variance $var (X)$ \n",
    "- for random initialization there is no correlation between the input and the weights \n",
    " - both should be approximatly zero-mean (through initialization resp. data preprocessing for $\\vec x$)\n",
    "\n",
    "So we use\n",
    " - $n$ is also called the \"fan out\" of a layer\n",
    " - $m$ is the \"fan in\" of a layer\n",
    "\n",
    "Now we want that the variance remains constant, i.e. same variance for input and output in the linear regime. So the following expression should be 1:\n",
    "\n",
    "$$\n",
    " \\frac{\\text{var}(\\vec x^T \\cdot \\vec w_i)}{\\text{var}(X)} = \n",
    " \\frac{\\text{var} (\\sum_{j=1}^n x_j  w_{ji})}{\\text{var}(X)}=\n",
    " \\frac{n {\\ }\\text{var}(X) \\text{var}(W)}{\\text{var}(X)} = \n",
    " n {\\ }\\text{var}(W) = 1\n",
    "$$\n",
    "\n",
    "\n",
    "i.e.:\n",
    "- var$(W) = 1/n$  \n",
    "resp.\n",
    "- std$(W) = 1/\\sqrt n$  \n",
    "\n",
    "With ReLu-Units only half of the units are in the acitive regime. So the variance of $W$ must be twice to yield the same effect, i.e.:\n",
    "- var$(W) = 2/n$  \n",
    "resp.\n",
    "- std$(W) = \\sqrt{2/n}$  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For training we do a forward pass and a backward pass. In the backward pass the error signal is [\"linearly\" backpropagated](http://christianherta.de/lehre/dataScience/machineLearning/neuralNetworks/feedforwardNeuralNetworks.html).\n",
    "\n",
    "[Glorot et al.](#GLO10) suggest taking the average between forward and backward pass for initialization, i.e.:\n",
    "- var$(W) = 2/(n + m)$  \n",
    "resp.\n",
    "- std$(W) = \\sqrt{2/(n+m)}$\n",
    "\n",
    "For ReLU's:\n",
    "- var$(W) = 4/(n + m)$  \n",
    "resp.\n",
    "- std$(W) = \\sqrt{4/(n+m)}$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises\n",
    "\n",
    "### Forward pass\n",
    "\n",
    "In this exercise we have some contrived `train_data` (1000 samples with 500 features). Implement the forward pass.\n",
    "* In each layer, the number of input and output features should remain the same.\n",
    "* The weights in each layer are drawn from the uniform distribution $[-1 .. 1]$\n",
    "* Each layer uses the `tanh` activation function.\n",
    "* Return the activations across all layers\n",
    "\n",
    "(We'll focus on the parameters `xavier` and `gain` in the next step)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "train_data = np.random.randn(1000,500)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def feed_forward(x,num_layers,xavier=False,gain=np.sqrt(2)):\n",
    "    raise NotImplementedError()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualise the activations\n",
    "\n",
    "\n",
    "Feed your `train_data` through the forward pass with 10 layers. Then plot the distribution of the activation values of each layer in a histogram. What does this tell you about the saturation of the network?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Some sample code that plots multiple histograms\n",
    "\n",
    "plt.figure(figsize=(40,20))\n",
    "\n",
    "def plot(activations):\n",
    "    plt.figure(figsize=(40,20))  \n",
    "    for i in range(10):\n",
    "        plt.subplot(3,4,i+1)\n",
    "        plt.hist(np.geomspace(0.1,2))\n",
    "        \n",
    "plot(feed_forward(train_data,num_layers=10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Xavier Initialization\n",
    "\n",
    "Update your implementation of `feed_forward`. If the parameter `xavier` is set to `True`, initialize all weights with Xavier initialization. Glorot et al. suggests the following normalized initialization\n",
    "\n",
    "$$\n",
    "W \\sim U \\left[ - \\frac{\\sqrt6}{\\sqrt{(fan\\_in+fan\\_out)}} , \\frac{\\sqrt6}{\\sqrt{(fan\\_in+fan\\_out)}} \\right]\n",
    "$$\n",
    "\n",
    "To put it into words: fan_in and fan_out are the number of input features and output features. Weights are drawn from the uniform distribution \n",
    "`-sqrt(6/(fan_in+fan_out))` to `sqrt(6/(fan_in+fan_out))`, multiplied by a constant `gain`.\n",
    "\n",
    "\n",
    "Repeat the forward pass and plot the activations using Xavier initialization - Does this solve the problem of saturation?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "plot(feed_forward(train_data,10,xavier=True))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using the neural net framework\n",
    "\n",
    "Now we turn towards implementing custom initializers in the neural net framework.\n",
    "\n",
    "First, create a model for a classification problem. We'll use the [breast cancer dataset](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_breast_cancer.html#). Define the following architecture:\n",
    "\n",
    "* First layer: Linear, 30 input features, 20 output features, tanh activation\n",
    "* Second layer: Linear, 20 input features, 10 output features, tanh activation\n",
    "* Third layer: Linear, 10 input features, 1 output feature, sigmoid activation\n",
    "* For the loss function, use cross-entropy.\n",
    "\n",
    "**Note:** Your neural net implementation may not have a `Tanh_Layer` function to return a layer that performs a matrix multiplication followed by a tanh function. But equivalently, you can use a linear layer and apply  the activation function in the forward pass, for example:\n",
    "\n",
    "```python\n",
    "def __init__():\n",
    "    self.hidden0 = self.Linear_Layer(...)\n",
    "   \n",
    "def forward(self,x):\n",
    "    return self.hidden0(x).tanh()```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x_train,y_train = datasets.load_breast_cancer(return_X_y=True)\n",
    "x_train = preprocessing.scale(x_train)\n",
    "#print(x_train)\n",
    "#print(y_train)\n",
    "print(x_train.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Net(Model):\n",
    "    def __init__(self):\n",
    "        super(Net,self).__init__()\n",
    "        # create layers\n",
    "        raise NotImplementedError()\n",
    "        \n",
    "    def loss(self,x,y):\n",
    "        if not type(y) == Node:\n",
    "            y = Node(y)\n",
    "        # compute and return cross entropy loss, accumulated over all samples\n",
    "        raise NotImplementedError()\n",
    "        \n",
    "    def forward(self, x):\n",
    "        if not type(x) == Node:\n",
    "            x = Node(x)\n",
    "        # implement the forward pass\n",
    "        # hidden_0 -> tanh -> hidden_1 -> tanh -> hidden_2 -> sigmoid\n",
    "        raise NotImplementedError()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Implement Initializer\n",
    "\n",
    "`Initializer` is an abstract class. Its method `initialize` iterates over all weights and biases in the network and sets their values.\n",
    "\n",
    "Any subclass represents a specific initialization method, e.g. Xavier. A subclass implements the methods `initial_weights(self, fan_in, fan_out)` and `initial_bias(self, fan_in, fan_out)`. The arguments `fan_in` and `fan_out` are the number of input and output features of the layer. The functions return initialized weights and bias suited for the layer, respectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Initializer():\n",
    "    def __init__(self):\n",
    "        pass\n",
    "        \n",
    "    def initialize(self,net):\n",
    "        for k,v in net.get_param().items():\n",
    "            fan_in,fan_out = v.shape\n",
    "            if 'weight' in k:\n",
    "                W = self.initial_weights(fan_in,fan_out)\n",
    "                np.copyto(v, W)\n",
    "            elif 'bias' in k:\n",
    "                b = self.initial_bias(fan_in, fan_out)\n",
    "                np.copyto(v, b)\n",
    "                \n",
    "    def initial_weights(self, fan_in, fan_out):\n",
    "        raise NotImplementedError('Must be implemented by subclass')\n",
    "    \n",
    "    def initial_bias(self, fan_in, fan_out):\n",
    "        raise NotImplementedError('Must be implemented by subclass')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Task:** Implement a few different initializers.\n",
    "* LowInitializer: initializes all parameters close to 0\n",
    "* LargeInitializer: initializes parameters at a large value e.g. random numbers drawn from the uniform distribution [-100..100]\n",
    "* NormalInitializer: initializes parameters with values drawn from a normal distribution (as opposed to a uniform distribution)\n",
    "* XavierInitializer: initializes parameters using Xavier initialization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class LowInitializer(Initializer):\n",
    "    def initial_weights(self, fan_in, fan_out):\n",
    "        pass\n",
    "    def initial_bias(self, fan_in, fan_out):\n",
    "        pass\n",
    "    \n",
    "class LargeInitializer(Initializer):\n",
    "    def initial_weights(self, fan_in, fan_out):\n",
    "        pass\n",
    "    def initial_bias(self, fan_in, fan_out):\n",
    "        pass\n",
    "    \n",
    "class NormalInitializer(Initializer):\n",
    "    def initial_weights(self, fan_in, fan_out):\n",
    "        pass\n",
    "    def initial_bias(self, fan_in, fan_out):\n",
    "        pass\n",
    "    \n",
    "class XavierInitializer(Initializer):\n",
    "    def initial_weights(self, fan_in, fan_out):\n",
    "        pass\n",
    "    def initial_bias(self, fan_in, fan_out):\n",
    "        pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Repeat the training process with different initializers applied to the network. Compare how well the network learns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "net = Net()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "#LowInitializer().initialize(net)\n",
    "LargeInitializer().initialize(net)\n",
    "#XavierInitializer().initialize(net)\n",
    "#NormalInitializer().initialize(net)\n",
    "optimiser = Adam(\n",
    "    net,\n",
    "    x_train=x_train,\n",
    "    y_train=y_train,\n",
    "    hyperparam = {\"alpha\": 0.01}\n",
    ")\n",
    "optimiser.train(steps=100,print_each=10);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Literature\n",
    "\n",
    "\n",
    "<table>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"GLO10\"></a>[GLO10]\n",
    "        </td>\n",
    "        <td>\n",
    "            X. Glorot, Y. Bengio: Understanding the difficulty of training deep feedforward neural networks, AISTATS 2010 <a href='http://jmlr.org/proceedings/papers/v9/glorot10a/glorot10a.pdf'>http://jmlr.org/proceedings/papers/v9/glorot10a/glorot10a.pdf</a>\n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"HE15\"></a>[HE15]\n",
    "        </td>\n",
    "        <td>\n",
    "            Kaiming He, Xiangyu Zhang, Shaoqing Ren, Jian Sun: Delving Deep into Rectifiers: Surpassing Human-Level Performance on ImageNet Classification <a href='http://arxiv.org/abs/1502.01852'>http://arxiv.org/abs/1502.01852</a>, IEEE International Conference on Computer Vision (ICCV), 2015 \n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"LAR15\"></a>[LAR15]\n",
    "        </td>\n",
    "        <td>\n",
    "            Larsson, Gustav: Initialization of deep networks <a href='http://deepdish.io/2015/02/24/network-initialization'>http://deepdish.io/2015/02/24/network-initialization/</a>, published 24 Feb 2015, accessed 01 June 2019\n",
    "        </td>\n",
    "    </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "_Notebook title_ <br/>\n",
    "by _[Benjamin Voigt](https://www.htw-berlin.de/forschung/promotion/laufende-promotionen/voigt-benjamin/), Diyar Oktay_ <br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2019 _[Benjamin Voigt](https://www.htw-berlin.de/forschung/promotion/laufende-promotionen/voigt-benjamin/), Diyar Oktay_\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
