import unittest
import dp
import numpy as np
from sklearn import datasets
from sklearn import preprocessing

# Test auto diff operations with the `Node` class
class AutodiffTest(unittest.TestCase):
    # Multiply two named scalar nodes and check the gradients
    def test_mul_scalar(self):
        a = dp.Node(2, 'A')
        b = dp.Node(3, 'B')
        c = a * b
        grads = c.grad(1)
        assert c.value.item() == 6
        assert grads['A'] == 3
        assert grads['B'] == 2

    def test_broadcasting(self):
        # Add a row vector to a matrix, the vector should
        # be broadcast along each row
        a = dp.Node(np.ones((1,10)))
        b = dp.Node(np.random.randn(20,10))
        c = a + b
        assert np.array_equal(c.value, b.value + a.value)


    def test_branching(self):
        # The node a splits into two nodes (b,c). Then
        # (b,c) join into d. Test if gradients are combined
        # properly
        a = dp.Node(3,'A')
        b = a * 2
        c = a.square()
        d = (b + c)
        assert d.value.item() == 15
        grads = d.grad(np.ones((1,1)))
        assert grads['A'].item() == 8


    def test_invalid_operations(self):
        # Shouldn't be broadcast
        a = dp.Node(np.random.randn(1,10))
        b = dp.Node(np.random.randn(3,4))
        with self.assertRaises(ValueError):
            a * b


# run tests on the `NeuralNode` class
class ModelTest(unittest.TestCase):
    def test_unknown_initializer(self):
        # Previously using an unrecognised string for the 'initializer'
        # argument would throw an error
        model = dp.Model()
        model.Linear_Layer(100,100,name='layer',initializer='Xaavier')

    @unittest.skip("This test fails - With the current API, you cannot modify the `gain` parameter of the Xavier initializer, it is statically set as sqrt(2)")
    def test_xavier_initialization(self):
        model = dp.Model()
        layers = []
        # add 5 layers with xavier init
        for i in range(5):
            layers.append(model.Linear_Layer(100,100,f'lay{i}',initializer='Xavier'))
        # fake data
        D = np.random.normal(loc=0,scale=1,size=(10,100))   
        out = dp.Node(D)
        means = []
        stds = []
        for fw in layers:
            out = fw(out)
            means.append(out.value.mean())
            stds.append(out.value.std())
        means,stds = np.array(means), np.array(stds)

        # If we could set the parameter `gain` of Xavier
        # to 1 rather thatn the defaulr sqrt(2) this would pass.
        # But with the current API this is not possible.
        np.testing.assert_allclose(means,0,atol=0.1)
        np.testing.assert_allclose(stds,1,atol=0.1)

    def test_no_duplicate_names(self):
        model = dp.Model()
        model.Linear_Layer(10,10,'name')
        with self.assertRaises(AssertionError):
            model.Linear_Layer(10,10,'name')
    
    def test_param_names_and_shapes(self):
        model = dp.Model()
        model.Linear_Layer(784,100,'h1')
        model.ReLu_Layer(100,10,'h2')
        assert set(model.get_param().keys()) == set(('h1_weight', 'h2_weight', 'h1_bias', 'h2_bias'))
        assert model.get_param()['h1_weight'].shape == (784,100)
        assert model.get_param()['h1_bias'].shape == (1,100)
        assert model.get_param()['h2_weight'].shape == (100,10)
        assert model.get_param()['h2_bias'].shape == (1,10)

class BreastCancerNet(dp.Model):    
    def __init__(self):
        super(BreastCancerNet, self).__init__()
        self.h = self.Linear_Layer(30, 20, "h0")
        self.h2 = self.Linear_Layer(20, 10, "h1")
        self.h3 = self.Linear_Layer(10, 1, "h2")
        
    def loss(self, x, y):
        if not type(y) == dp.Node:
            y = dp.Node(y)
        out = self.forward(x)
        loss = -1 * (y * out.log() + (1 - y) * (1 - out).log())
        return loss.sum()
    
    def forward(self, x):
        if not type(x) == dp.Node:
            x = dp.Node(x)
        x = self.h(x).tanh()
        x = self.h2(x).tanh()
        out = self.h3(x).sigmoid()
        
        return out 

class LearnTest(unittest.TestCase):
    """A model is set up for the breast cancer
    dataset. Test if the learning process is successful."""
    def setUp(self):
        X,y = datasets.load_breast_cancer(return_X_y=True)
        X = preprocessing.normalize(X)
        self.X = X
        self.y = y

    def optimizer_learns(self, Optimizer):
        """Parameter `Optimizer` : Class of the optimizer to test

        This test creates a model for the breast cancer dataset and trains
        it using the supplied optimiser class.
        Then, we look at the loss history of the training process and check if it
        tends to go down."""
        model = BreastCancerNet()
        optim = Optimizer(model,self.X,self.y)
        loss, loss_hist, param_hist = optim.train(steps=300,err_hist=True,print_each=100000)
        loss_hist = np.array(loss_hist)
        # difference between the loss at the i-th iteration with the loss at the iteration 
        # three steps earlier. if the value in `delta` is negative, that means the loss
        # went down.
        delta = loss_hist[1::3] - loss_hist[:1:3]
        # the values in `delta` should be mostly negative.
        return np.mean(delta < 0) > 0.8

    def test_predict(self):
        model = BreastCancerNet()
        out = model.forward(self.X).value
        assert np.all(out <= 1.0)
        assert np.all(out >= 0.0)
        
    def test_optimizers(self):
        assert self.optimizer_learns(dp.SGD)
        assert self.optimizer_learns(dp.SGD_Momentum)
        assert self.optimizer_learns(dp.RMS_Prop)
        assert self.optimizer_learns(dp.Adam)
    

if __name__ == '__main__' :
    unittest.main()
