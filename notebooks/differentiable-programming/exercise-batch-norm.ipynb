{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Batch Norm (Differentiable Programming)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "\n",
    "* [Introduction](#Introduction)\n",
    "* [Requirements](#Requirements)\n",
    "  * [Knowledge](#Knowledge)\n",
    "  * [Python Modules](#Python-Modules)\n",
    "* [Data](#Data)\n",
    "* [Batch normalization](#Batch-normalization)\n",
    "* [Exercises](#Exercises)\n",
    "  * [Implement batch norm](#Implement-batch-norm)\n",
    "  * [New data](#New-data)\n",
    "  * [Gradient descent](#Gradient-descent)\n",
    "* [Literature](#Literature)\n",
    "* [Licenses](#Licenses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "This notebook deals with 'Batch Normalization'. You're likely familiar with feature scaling - transform all features of the input data to roughly the same range before feeding it into the network.\n",
    "\n",
    "**Batch normalization** takes this a step further and performs normalization on the activations at each layer."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Requirements\n",
    "\n",
    "### Knowledge\n",
    "It's not required to study these resources before tackling this notebook but they provide an excellent coverage of the topic.\n",
    "* The original [Batch Norm paper](https://arxiv.org/pdf/1502.03167v3.pdf) by S. Ioffe/C. Szegedy [[IOF15]](#IOF15)\n",
    "* The write-up [Batch Norm layer](https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/batch_norm_layer.html) by Leonardo Araujo dos Santos [[ARA18]](#ARA18)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Python Modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import dp\n",
    "from dp import NeuralNode,Node\n",
    "import numpy as np\n",
    "from sklearn import datasets,preprocessing\n",
    "from sklearn.model_selection import train_test_split\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data\n",
    "\n",
    "This cell downloads the [breast cancer dataset](https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_breast_cancer.html#) provided by [sklearn](https://scikit-learn.org/stable/index.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x,y = datasets.load_breast_cancer(return_X_y=True)\n",
    "x = preprocessing.scale(x)\n",
    "x_train, x_test, y_train, y_test = train_test_split(x,y,test_size=0.2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Batch normalization\n",
    "\n",
    "Imagine a deep neural network attempting to learn. Before we feed our data into the network, we normalize each dimension to have a mean of 0 and a standard deviation of 1. To do so we subtract the expected value (mean) $E$ and divide by the sqrt of the variance $Var$ of each dimension.\n",
    "$$\n",
    "x^{norm} = \\frac{x - E[x]}{\\sqrt{Var(x)}}\n",
    "$$\n",
    "It's common to add a small number $\\epsilon$ to the variance just to prevent taking the square root of zero. In python code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# data\n",
    "foo = np.random.randn(1000,20)\n",
    "\n",
    "# mean and variance per feature\n",
    "mean = foo.mean(axis=0,keepdims=True)\n",
    "var = foo.var(axis=0,keepdims=True)\n",
    "epsilon = 1e-8\n",
    "\n",
    "# normalization\n",
    "foo_norm = (foo - mean)/np.sqrt(var + epsilon)\n",
    "foo_norm.mean(), foo_norm.std()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So the first layer is happy and content, its input is always normalized. But what about the deeper layers? After the data has gone through multiple matrix multiplications in the network its mean and standard deviation have likely shifted.\n",
    "\n",
    "On top of that, in each iteration of learning, the parameters of the first layer change, so the succeeding layers try to learn on data with constantly shifting mean and variance.\n",
    "\n",
    "To make things easier for the deeper layers, batch normalization is applied. Each layer calculates the mean and variance of each feature over the mini-batch of samples `x`. Each sample is then normalized as\n",
    "```python\n",
    "x_norm = (x - batch_mean)/batch_variance\n",
    "```\n",
    "Then, we essentially provide a customizable standard deviation through the **hyperparameters** $\\gamma$ (gamma) and $\\beta$ (beta)\n",
    "```python\n",
    "out = gamma * x_norm + beta\n",
    "```\n",
    "These parameters $\\gamma$ and $\\beta$ are learnable in the training process. You update them as you would any other parameter such as the weight of a linear layer, e.g. with SGD, Momentum or Adam."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises\n",
    "\n",
    "### Bias\n",
    "\n",
    "**Task:**\n",
    "\n",
    "A linear layer generally performs the forward pass $x \\cdot W + b$. Show that if we apply batch norm after a linear layer, we can omit the bias term $b$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Implement batch norm\n",
    "We'll use the `Node` class in dp.py for automatic differentiation and create a model for the breast cancer dataset. The architecture should look as follows\n",
    "```fsharp\n",
    "data(30 features) -> linear(30,20) -> batch norm -> tanh -> linear(20,10) -> batch norm -> tanh -> linear(10,1) -> sigmoid \n",
    "```\n",
    "**Task:**\n",
    "\n",
    "Implement the method `batch_norm` to add a batch norm layer to the network."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Model():\n",
    "    # define layers of the model\n",
    "    def __init__(self):\n",
    "        self.params = dict()\n",
    "        self.fc1 = self.linear(30,20,'fc1')\n",
    "        self.bn1 = self.batch_norm(20,'bn1')\n",
    "        self.fc2 = self.linear(20,10,'fc2')\n",
    "        self.bn2 = self.batch_norm(10,'bn2')\n",
    "        self.fc3 = self.linear(10,1,'fc3')\n",
    "    \n",
    "    # define forward pass\n",
    "    def forward(self,x,train=True):\n",
    "        if not type(x) == Node:\n",
    "            x = Node(x)\n",
    "        x = self.fc1(x)\n",
    "        x = self.bn1(x).tanh()\n",
    "        x = self.fc2(x)\n",
    "        x = self.bn2(x).tanh()\n",
    "        x = self.fc3(x)\n",
    "        out = x.sigmoid()\n",
    "        return out\n",
    "        \n",
    "    # define loss function\n",
    "    def loss(self,x,y):\n",
    "        out = self.forward(x,train=True)\n",
    "        if not type(y) == Node:\n",
    "            y = Node(y)\n",
    "        loss = -1 * (y * out.log() + (1 - y) * (1 - out).log())\n",
    "        return loss.sum()\n",
    "    \n",
    "    # add a linear layer to the model\n",
    "    def linear(self, fan_in,fan_out,name):\n",
    "        W_name, W_value = f'weight_{name}', np.random.randn(fan_in,fan_out)\n",
    "        self.params[W_name] = W_value\n",
    "        \n",
    "        def forward(x):\n",
    "            return x.dot(Node(self.params[W_name], W_name))\n",
    "\n",
    "        return forward"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def batch_norm(self, fan_in, name):\n",
    "    # TODO: add gamma and beta of this layer to self.params\n",
    "    # TODO: define and return the forward pass, i.e. a function that\n",
    "    #       applies batch norm to x\n",
    "    raise NotImplementedError()\n",
    "    \n",
    "Model.batch_norm = batch_norm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Verify if your implementation works properly. The output of the batch norm layer should have a mean of **beta** and a standard deviation of **gamma**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "net = Model()\n",
    "assert 'gamma_bn1','beta_bn1' in net.params\n",
    "assert 'gamma_bn2','beta_bn2' in net.params\n",
    "\n",
    "data = np.random.randn(100,10)\n",
    "out = net.bn2(Node(data)).value\n",
    "print(out.mean(axis=0))\n",
    "print(out.std(axis=0))\n",
    "assert np.allclose(out.mean(axis=0), net.params['beta_bn2'], atol=1e-5)\n",
    "assert np.allclose(out.std(axis=0), np.abs(net.params['gamma_bn2']), atol=1e-5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### New data\n",
    "Recall: During the training process, you calculate the mean and variance of each feature **over the mini batch** of samples, then normalize each sample as\n",
    "```python\n",
    "x_norm = (x - batch_mean)/batch_variance.\n",
    "```\n",
    "\n",
    "After training is completed, you may want to classify a single sample or a whole dataset, so there are no mini-batches.\n",
    "\n",
    "To account for this, during the learning process you keep track of the **moving average** of the batch mean and batch variance. This moving average is then applied to normalize non-train data. If moving averages are new to you you may want to check out this [Notebook on optimizers](./exercise_optimizers.ipynb).\n",
    "\n",
    "\n",
    "**Task:** \n",
    "\n",
    "Change your implementation of the `batch_layer` method to add `avg_mean_{layer_name}` and `avg_variance_{layer_name}` to the parameters of the model. For each mini-batch that the network sees during training, update the parameters as the moving average of the batch mean and batch variance, respectively.\n",
    "\n",
    "**Note:** The forward function returned by the `batch_norm` method needs a parameter such as `train` to distinguish between train batches and test samples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def batch_norm(self, fan_in,name):\n",
    "    raise NotImplementedError()\n",
    "\n",
    "    def forward(x,train=True):\n",
    "        raise NotImplementedError()\n",
    "    \n",
    "    return forward\n",
    "    \n",
    "Model.batch_norm = batch_norm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Verify your implementation: This tests feeds data with a mean of 42 and a standard deviation of 10 through the batch norm layer many times and checks the moving averages the layer has learned."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "net = Model()\n",
    "assert np.all(net.params['avg_mean_bn1'] == 0)\n",
    "assert np.all(net.params['avg_variance_bn2'] == 0)\n",
    "for i in range(100):\n",
    "    data = Node(np.random.normal(loc=42,scale=10,size=((1000,20))))\n",
    "    net.bn1(data)\n",
    "np.testing.assert_allclose(42, net.params['avg_mean_bn1'], atol=1)\n",
    "np.testing.assert_allclose(10**2, net.params['avg_variance_bn1'], atol=5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Gradient descent\n",
    "This cell creates a simple training loop to train and then test the model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "net = Model()\n",
    "\n",
    "lrate = 0.01\n",
    "batch_size = 50\n",
    "steps = 100\n",
    "\n",
    "# training\n",
    "for i in range(steps):\n",
    "    minis = np.random.choice(np.arange(len(x_train)),size=batch_size, replace=False)\n",
    "    x_mini = x_train[minis,:]\n",
    "    y_mini = y_train[minis]\n",
    "    loss = net.loss(x_mini,y_mini)\n",
    "    grads = loss.grad(1)\n",
    "    new_params = { k : net.params[k] - lrate * grads[k]\n",
    "                 for k in grads.keys() }\n",
    "    net.params.update(new_params)\n",
    "    \n",
    "# testing\n",
    "pred = np.round(net.forward(x_test).value).squeeze()\n",
    "np.mean(pred == y_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Literature\n",
    "\n",
    "<table>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"IOF15\"></a>[IOF15]\n",
    "        </td>\n",
    "        <td>\n",
    "            JS. Ioffe and C. Szegedy, “Batch Normalization: Accelerating Deep Network Training by Reducing Internal Covariate Shift,” arXiv:1502.03167 [cs.LG], Mar. 2015. \n",
    "        </td>\n",
    "    </tr>\n",
    "    <tr>\n",
    "        <td>\n",
    "            <a name=\"ARA18\"></a>[ARA18]\n",
    "        </td>\n",
    "        <td>\n",
    "            L. A. dos Santos, “Batch Norm layer,” Batch Norm layer · Artificial Inteligence, 2018. [Online]. Available: <a href='https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/batch_norm_layer.html'>https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/batch_norm_layer.html</a>. [Accessed: 15-Jun-2019]. \n",
    "        </td>\n",
    "    </tr>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Licenses\n",
    "\n",
    "### Notebook License (CC-BY-SA 4.0)\n",
    "\n",
    "*The following license applies to the complete notebook, including code cells. It does however not apply to any referenced external media (e.g., images).*\n",
    "\n",
    "_Batch Normalization_ <br/>\n",
    "by _Diyar Oktay_ <br/>\n",
    "is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).<br/>\n",
    "Based on a work at https://gitlab.com/deep.TEACHING.\n",
    "\n",
    "\n",
    "### Code License (MIT)\n",
    "\n",
    "*The following license only applies to code cells of the notebook.*\n",
    "\n",
    "Copyright 2018 _Diyar Oktay_\n",
    "\n",
    "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n",
    "\n",
    "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "deep_teaching_kernel",
   "language": "python",
   "name": "deep_teaching_kernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
